# UIHelper

[![CI Status](http://img.shields.io/travis/Pavan/UIHelper.svg?style=flat)](https://travis-ci.org/Pavan/UIHelper)
[![Version](https://img.shields.io/cocoapods/v/UIHelper.svg?style=flat)](http://cocoapods.org/pods/UIHelper)
[![License](https://img.shields.io/cocoapods/l/UIHelper.svg?style=flat)](http://cocoapods.org/pods/UIHelper)
[![Platform](https://img.shields.io/cocoapods/p/UIHelper.svg?style=flat)](http://cocoapods.org/pods/UIHelper)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UIHelper is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UIHelper"
```

## Author

Pavan, pavan.itagi@ymedialabs.com

## License

UIHelper is available under the MIT license. See the LICENSE file for more info.
